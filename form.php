<!DOCTYPE html>
<html>
<head>   
<title> Membership Form
</title> 
<link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;500&display=swap" rel="stylesheet"> 
<style >
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}
*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}
html,body{
	background-color:#fff ;
	color: #fff;
	text-rendering: optimizeLegibility;
	font-size: 16px;
	font-family: 'Work Sans','Arial',sans-serif;
	font-weight: 400;
}
h1{
	color: #ed07b7;
	font-weight: 1500;
	font-size: 40px;

}
h3{
	color: #ed07b7;
}
input[type=text], input[type=email],textarea, input[type=radio], select {
  
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 10px;
  box-sizing: border-box;
}
label{
	font-weight: 600;
	font-size: 23px;
}
.img{
	background-image:linear-gradient(rgba(0,0,0,.1),rgb(0,0,0)) ,url(form.jpg); 
	background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  }
.text{
	text-align: center;
}
.col {
	display: block;
	margin: 1% 0 2% 1.6%;
	text-align: center;
}
.row {
	display: flex;
	
	justify-content: space-between;
}
.btn{
	text-decoration: none;
	padding: 10px 30px;
	border-radius: 200px;
	text-align: center;
	size: 2000px;
}
.btn-full{
	background-color: #ed07b7;
	color: #fff;
	text-align: center;
}
</style>
</head>  
<body > 

<div class="img">
<div class="col">
	<div class="text">
 
<h1><u>BE A PART OF OUR COMMUNITY TODAY!</u></h1><br>
<h2>We offer a membership option tailored to meet your needs.</h2><br><h2> To apply, please fill in and submit the form below.</h2><br>  
<h2>If you would like more information on getting a membership in our organization or the application process, please ping us at the <a href="main.php #contact">Contact Us</a> section. We will get back to you soon!</h2><br><br>
</div>
<form action="res.php" method="post" id="Form">  
  
<label> Full Name</label> <br>        
<input type="text" name="fullname" id="fullname" size="20" placeholder="Your Name" required> <br> <br> <br>

<label>Age </label> <br>
<input type="text" name="age" id="age" size="3" required>  <br><br><br>

<label>Gender</label>&nbsp &nbsp
<input type="radio" id="gender" name="gender" value="Male">Male 
<input type="radio" id ="gender" name="gender" value="Female"> Female  
<input type="radio" id="gender" name="gender" value="Other"> Other  
<br><br><br> 
 
<label>Phone</label>  <br>
<input type="text" name="country code" value="+91" size="1">   
<input type="text" name="phone" id="phone" size="12" required> <br> <br> <br> 

<label>Address</label>  
<br>  
<textarea cols="80" rows="5" name="address" id="address" placeholder="Your Address" required>  </textarea>  
<br> <br> <br>

<label>Email</label><br>
<input type="email" id="email" name="email" placeholder="Your email" required> 
<br> <br> <br>

<label>Interests</label><br>
<select name="interests" id="interests">
<option value="Golf">Golf</option>
<option value="Tennis">Tennis</option>
<option value="Pool">Pool</option>
<option value="Shuttle">Shuttle</option>
<option value="Swimming">Swimming</option>
<option value="Cricket">Cricket</option>
<option value="Football">Football</option>
</select>
<br><br><br>

<h2> Memberships that we offer :</h2><br>
<h3> Platinum Membership </h3>
<p>You'll be entitled to a wide range of benefits. You will get access to all the facilities in our club.</p><br>
<h3> Gym Membership </h3>
<p>For people who are interested in enjoying our gym facilities. We provide spa services as complementary.</p><br>
<h3>Silver Membership</h3>
<p>You will have access to all sports and dining facilities. </p> <br>
<p>For more information on membership, visit our <a href="main.php #membership">membership</a> page.</p><br>

<label> Choose Membership </label><br><br>
<input type="radio" id="member" name="member" value="Platinum"> Platinum membership <br><br>
<input type="radio" id="member" name="member" value="Gym"> Gym Membership  <br><br>
<input type="radio" id="member" name="member" value="Silver"> Silver Membership <br><br>
<br> <br> 
<input type="button" class="btn btn-full" value="Reset" onclick="myFunction()"> &nbsp &nbsp
 <input type="submit" class="btn btn-full" name="create" id="Btn" value="Submit"> 
</form>  
</div> </div></div>

<script type="text/javascript" src="script.js"></script>

</body>  
</html>  