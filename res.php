<?php

$fullname=$_POST['fullname'];
$age=$_POST['age'];
$gender=$_POST['gender'];
$phone=$_POST['phone'];
$address=$_POST['address'];
$email=$_POST['email'];
$interests=$_POST['interests'];
$member=$_POST['member'];

?>


<!DOCTYPE html>
<html>
<head>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;500&display=swap" rel="stylesheet"> 

<style>
html,body{
  background-color:#dddddd;
  color: #000;
  text-rendering: optimizeLegibility;
  font-size: 16px;
  font-family: 'Work Sans','Arial',sans-serif;
  font-weight: 400;
}
h1{
  color: #ed07b7;
  font-weight: 1500;
  font-size: 40px;

}
table {
  font-family: 'Work Sans','Arial',sans-serif;
  font-size: 20px;
  font-weight: 500;
  border-collapse: collapse;
  width: 50%;
}
.center {
  margin-left: auto;
  margin-right: auto;
}
td, th {
  border: 2px solid #000;
  text-align: center;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #808080;
}
.text{
  text-align: center;
}
</style>
</head>
<body>

<div class="text">
<h1>Thank You For Joining Our Club!!</h1>
<h2>Your Registration Details</h2>
</div>
<table class="center">
  <tr>
    <td>Name</td>
    <td><?php echo $fullname; ?></td>
    
  </tr>
  <tr>
    <td>Age</td>
    <td><?php echo $age; ?></td>
    
  </tr>
  <tr>
    <td>Gender</td>
    <td><?php echo $gender; ?></td>
    
  </tr>
  <tr>
    <td>Phone</td>
    <td><?php echo $phone; ?></td>
    
  </tr>
  <tr>
    <td>Address</td>
    <td><?php echo $address; ?></td>
    
  </tr>
  <tr>
    <td>Email</td>
    <td><?php echo $email; ?></td>
    
  </tr>
  <tr>
    <td>Interests</td>
    <td><?php echo $interests; ?></td>
    
  </tr>
  <tr>
    <td>Member</td>
    <td><?php echo $member; ?></td>
    
  </tr>
  
</table>

</body>
</html>
