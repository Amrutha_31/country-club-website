# Country Club Website

The website is for a country club with all details about the social and recreational activities, golf, pool, tennis, etc that they provide along with the membership details and prices. With that information you can register for the country club.

**Before running the file, make sure to connect to server**

**Run the main.html file after starting the server for the website to work and make sure all files are present in the localhost directory of server.**

**main.html is the main html file for the website** - 
Run it on the server and it displays the website front page with a header, about section, club amenities section, testimonials section, membership section, contact us section and a footer.

**script..css is the css file used for styling the website**

**script.js is the javascript file used in the membership registration form (form.php)**

**form.php is the php file used for regsitering a member for the club by selecting the membership plans** - 
On clicking 'Sign Up' button or 'Become a member' button in the main.html file, one will be able to move to this form.php file for registering. 

**res.php is the php file which displays the details of the registered member**

**Video of the website has also been uploaded in the videos folder**


**form1.html and form2.html are the initial stage of the html form and website creation** - 
form1.html is the membership form where the user has to input his personal details and preferences and then select an appropriate membership for the club. 
form2.html is the contact form where the user can ask any queries or give his feedback.
